﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromocodeMapper : IPromocodeMapper
    {
        public PromoCodeShortResponse PromocodeToShortResponse(PromoCode promocode)
        {
            return new PromoCodeShortResponse()
            {
                Id = promocode.Id,
                Code = promocode.Code,
                ServiceInfo = promocode.ServiceInfo,
                BeginDate = promocode.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = promocode.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = promocode.PartnerName
            };
        }

        public PromoCode PromocodeFromRequest(GivePromoCodeRequest request, Preference preference, Customer customer, Guid id)
        {

            var promocode = new PromoCode();
            promocode.Id = id;

            promocode.PartnerName = request.PartnerName;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;

            promocode.BeginDate = DateTime.Now;
            promocode.EndDate = DateTime.Now;

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            if (customer != null)
            {
                promocode.Customer = customer;
                promocode.CustomerId = customer.Id;
            }
            return promocode;
        }

    }
}
