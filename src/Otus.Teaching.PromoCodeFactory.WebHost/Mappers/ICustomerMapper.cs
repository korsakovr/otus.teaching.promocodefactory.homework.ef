﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public interface ICustomerMapper
    {
        CustomerShortResponse CustomerToShortResponse(Customer customer);
        CustomerResponse CustomerToResponse(Customer customer);
        Customer RequestToCustomer(CreateOrEditCustomerRequest request, Guid id);
        Customer RequestToCustomer(CreateOrEditCustomerRequest request, Customer customer);
    }
}
