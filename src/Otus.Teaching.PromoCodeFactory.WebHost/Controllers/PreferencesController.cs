﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IPreferenceMapper _preferenceMapper;
        public PreferencesController(IRepository<Preference> preferenceRepository,
            IPreferenceMapper preferenceMapper)
        {
            _preferenceRepository = preferenceRepository;
            _preferenceMapper = preferenceMapper;
        }

        /// <summary>
        /// Получить данные все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = preferences.Select(x => _preferenceMapper.PreferenceToResponse(x)).ToList();
            return Ok(response);
        }

    }
}
